# ToDo App: Mi primera app con AdonisJS

Resultado del tutorial: [#002 - ToDo App: Mi primera app con AdonisJS](http://www.victorvr.com/tutorial/todo-app-mi-primera-app-con-adonisjs)

Demo: [https://adonisjs-todo-app.herokuapp.com](https://adonisjs-todo-app.herokuapp.com)

![ToDo App](/public/app.gif)

## Requerimientos

Esta aplicación asume que tienes lo siguiente instalado en tu computadora:

`node >= 8.0` o mayor.

```bash
node --version
```

`npm >= 5.0` o mayor.

```bash
npm --version
```

## Instalación de Adonis CLI

Primero necesitamos instalar `Adonis CLI`:

```bash
npm i -g adonisjs-cli
```

## Instalación de Dependencias via NPM

Ahora instalaremos las dependencias de nuestra aplicación:

```bash
npm install
```

## Crear base de datos en MySQL

Continuamos creando la base de datos en `MySQL` llamada `adonisjs-todo-app`:

```bash
mysql -u usuario -p
mysql > CREATE DATABASE adonisjs-todo-app;
```

## Configurar variables de entorno

Configuramos las variables de entorno del archivo `.env`:

```bash
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_USER={TU_USUARIO}
DB_PASSWORD={TU_CONTRASEÑA}
DB_DATABASE=adonisjs-todo-app
```

## Ejecutar migración

Ejecutamos la migracion de la base de datos:

```bash
adonis migration:run
```

## Iniciar el servidor
Ejecutamos la aplicación:

```bash
adonis serve --dev
```

## Abrir la aplicación
Y por último, abrimos [http://localhost:3333](http://localhost:3333) en el navegador.